# CinemaDBGen

This is a tool used for generating fake data for databases.
It executes python code injected into the SQL script.

## Usage

The tool essentially executes injected python code in `.sql` files.
The executed code should be contained in a block such as this:
```
/*py
<your actual python code>
*/
```
These blocks, however, need to be inserted in correct places and the code may have to do some specific things.

We can distinguish two places of insertion of such blocks:
 - before a `CREATE TABLE` statement
 - after a field definition

The code before a `CREATE TABLE` statement will need to define two functions:
 - `tableContext()`
    This will be called once for the table.
    This should return a dictionary which contains data regarding the table as a whole.
 - `rowContext(tableCtx)`
    This will be called for each row created for the table.
    The first argument's values are the ones you returned from `tableContext()`.
    This should return a dictionary which contains data regarding a single row.

The code after a field definition will be executed as an expression.
It'll need to return a value which will be ultimately inserted into the table.


## Contributing

```
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
./build.sh
```

The `view-tree.sh` utility uses antlr4-tools to graphically display the parse tree.
Usage: `./view-tree.sh <mysql-file-path>`
