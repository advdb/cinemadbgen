START TRANSACTION;
-- Wstawienie danych do tabeli użytkownik
INSERT INTO `uzytkownik` (`uzytkownik_id`, `imie`, `nazwisko`, `email`, `haslo`) VALUES
(1, 'Jan', 'Kowalski', 'jan.kowalski@example.com', 'haslo123'),
(2, 'Anna', 'Nowak', 'anna.nowak@example.com', 'password'),
(3, 'Piotr', 'Wiśniewski', 'piotr.wisniewski@example.com', 'qwerty');

-- Wstawienie danych do tabeli pracownik z odwołaniem do tabeli użytkownik
INSERT INTO `pracownik` (`pracownik_id`, `pensja`, `czy_admin`) VALUES
(1, 5000.00, 1), 
(2, 4000.00, 0);
COMMIT;

START TRANSACTION;

INSERT INTO `lokalizacja` (`gps_x`, `gps_y`, `nazwa_miejscowosci`) VALUES
(52.2297, 21.0122, 'Warszawa'),
(50.0647, 19.9450, 'Kraków'),
(52.4064, 16.9252, 'Poznań');

INSERT INTO `kino` (`kino_id`, `nazwa_kina`) VALUES
(1, 'Multikino Warszawa'),
(2, 'Cinema City Kraków'),
(3, 'Helios Poznań');

INSERT INTO `sala` (`sala_id`, `nazwa_sali`) VALUES
(1, 'Sala A'),
(2, 'Sala B'),
(3, 'Sala C');

INSERT INTO `miejsce` (`rzad_id`, `sala_id`, `siedzenie`, `dla_niepelnosprawnych`) VALUES
(1, 1, 1, 0),
(1, 1, 2, 1),
(2, 1, 1, 0),
(2, 1, 2, 0),
(1, 2, 1, 0),
(1, 2, 2, 1),
(2, 2, 1, 0),
(2, 2, 2, 0),
(1, 3, 1, 0),
(1, 3, 2, 1),
(2, 3, 1, 0),
(2, 3, 2, 0);

COMMIT;

