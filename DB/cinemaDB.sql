CREATE DATABASE  IF NOT EXISTS `cinemadb` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `cinemadb`;
-- MySQL dump 10.13  Distrib 8.0.36, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: cinemadb
-- ------------------------------------------------------
-- Server version	8.3.0

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bilet`
--

DROP TABLE IF EXISTS `bilet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bilet` (
  `bilet_id` int NOT NULL AUTO_INCREMENT,
  `promocja_id` int DEFAULT NULL,
  `seans_id` int NOT NULL,
  `miejsce_id` int NOT NULL,
  `uzytkownik_id` int NOT NULL,
  `cena_koncowa` double NOT NULL,
  PRIMARY KEY (`bilet_id`),
  KEY `FKBilet786766` (`seans_id`),
  KEY `FKBilet62501` (`miejsce_id`),
  KEY `FKBilet102090` (`promocja_id`),
  KEY `FKBilet661674` (`uzytkownik_id`),
  CONSTRAINT `FKBilet102090` FOREIGN KEY (`promocja_id`) REFERENCES `promocja` (`promocja_id`),
  CONSTRAINT `FKBilet62501` FOREIGN KEY (`miejsce_id`) REFERENCES `miejsce` (`miejsce_id`),
  CONSTRAINT `FKBilet661674` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownik` (`uzytkownik_id`),
  CONSTRAINT `FKBilet786766` FOREIGN KEY (`seans_id`) REFERENCES `seans` (`seans_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `film`
--

DROP TABLE IF EXISTS `film`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `film` (
  `film_id` int NOT NULL AUTO_INCREMENT,
  `minuty_filmu` int NOT NULL,
  `dla_doroslych` tinyint NOT NULL,
  `tytul` varchar(255) NOT NULL,
  `rok_premiery` year NOT NULL,
  PRIMARY KEY (`film_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `kino`
--

DROP TABLE IF EXISTS `kino`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `kino` (
  `kino_id` int NOT NULL AUTO_INCREMENT,
  `nazwa_kina` varchar(255) NOT NULL,
  `lokalizacja_id` int NOT NULL,
  PRIMARY KEY (`kino_id`),
  KEY `FKKino504060` (`lokalizacja_id`),
  CONSTRAINT `FKKino504060` FOREIGN KEY (`lokalizacja_id`) REFERENCES `lokalizacja` (`lokalizacja_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lokalizacja`
--

DROP TABLE IF EXISTS `lokalizacja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lokalizacja` (
  `lokalizacja_id` int NOT NULL AUTO_INCREMENT,
  `gps_x` double NOT NULL,
  `gps_y` double NOT NULL,
  `nazwa_miejscowosci` varchar(255) NOT NULL,
  PRIMARY KEY (`lokalizacja_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `miejsce`
--

DROP TABLE IF EXISTS `miejsce`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `miejsce` (
  `miejsce_id` int NOT NULL AUTO_INCREMENT,
  `rzad_id` int NOT NULL,
  `sala_id` int NOT NULL,
  `siedzenie` int NOT NULL,
  `dla_niepelnosprawnych` tinyint NOT NULL,
  PRIMARY KEY (`miejsce_id`),
  KEY `FKMiejsce531702` (`sala_id`),
  CONSTRAINT `FKMiejsce531702` FOREIGN KEY (`sala_id`) REFERENCES `sala` (`sala_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pracownik`
--

DROP TABLE IF EXISTS `pracownik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pracownik` (
  `pracownik_id` int NOT NULL AUTO_INCREMENT,
  `pensja` double NOT NULL,
  `czy_admin` tinyint NOT NULL,
  PRIMARY KEY (`pracownik_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `promocja`
--

DROP TABLE IF EXISTS `promocja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `promocja` (
  `promocja_id` int NOT NULL AUTO_INCREMENT,
  `nazwa_promocji` varchar(255) NOT NULL,
  `znizka` double NOT NULL,
  PRIMARY KEY (`promocja_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sala`
--

DROP TABLE IF EXISTS `sala`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sala` (
  `sala_id` int NOT NULL AUTO_INCREMENT,
  `nazwa_sali` varchar(255) NOT NULL,
  `kino_id` int NOT NULL,
  PRIMARY KEY (`sala_id`),
  KEY `FKSala701999` (`kino_id`),
  CONSTRAINT `FKSala701999` FOREIGN KEY (`kino_id`) REFERENCES `kino` (`kino_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `seans`
--

DROP TABLE IF EXISTS `seans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `seans` (
  `seans_id` int NOT NULL AUTO_INCREMENT,
  `film_id` int NOT NULL,
  `sala_id` int NOT NULL,
  `rozpoczecie_seansu` datetime NOT NULL,
  `cena` double NOT NULL,
  PRIMARY KEY (`seans_id`),
  KEY `FKSeans719584` (`sala_id`),
  KEY `FKSeans578135` (`film_id`),
  CONSTRAINT `FKSeans578135` FOREIGN KEY (`film_id`) REFERENCES `film` (`film_id`),
  CONSTRAINT `FKSeans719584` FOREIGN KEY (`sala_id`) REFERENCES `sala` (`sala_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `uzytkownik`
--

DROP TABLE IF EXISTS `uzytkownik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `uzytkownik` (
  `uzytkownik_id` int NOT NULL AUTO_INCREMENT,
  `imie` varchar(255) NOT NULL,
  `nazwisko` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `haslo` varchar(255) NOT NULL,
  `pracownik_id` int DEFAULT NULL,
  PRIMARY KEY (`uzytkownik_id`),
  KEY `FKUzytkownik706644` (`pracownik_id`),
  CONSTRAINT `FKUzytkownik706644` FOREIGN KEY (`pracownik_id`) REFERENCES `pracownik` (`pracownik_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-05-20 13:52:35
