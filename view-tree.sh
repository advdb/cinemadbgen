#!/usr/bin/env bash

if [ -z $1 ]; then
  echo 'Provide file to visualize as an argument'
  exit 1
fi

pip install -r requirements.txt

PARSER_LOCATION='src/parsers/antlr4src'
antlr4-parse "$PARSER_LOCATION/MySqlLexer.g4" "$PARSER_LOCATION/MySqlParser.g4" root -gui $1
