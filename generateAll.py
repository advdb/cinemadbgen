import json

import mysql.connector
from mysql.connector import Error
from faker import Faker
import random

fake = Faker()
Lokalizacja = []
Kino = []
Sala = []
Seans = []
Film = []
Bilet = []
Uzytkownik = []
Promocja = []
Pracownik = []
Miejsce = []
PomSeans = {}
PomProm = {}


def get_next_id(cursor, table_name, id_column, last_id):
    if last_id is None:
        query = f"""
            SELECT 
            COALESCE(MAX({id_column}), 0) + 1
            FROM {table_name}
        """
        cursor.execute(query)
        result = cursor.fetchone()
        last_id = result[0]
    else:
        last_id += 1
    return last_id


def get_random_id(cursor, table_name, id_column, table):
    query = f"SELECT {id_column} FROM {table_name}"
    cursor.execute(query)
    ids = cursor.fetchall()
    ids = [id[0] for id in ids]
    temp = ids + table
    unikalne_id = list(set(temp))
    return random.choice(unikalne_id)


def is_table_empty(cursor, table_name):
    query = f"SELECT COUNT(*) FROM {table_name}"
    cursor.execute(query)
    count = cursor.fetchone()[0]
    return count == 0

def pobierzDane(data, id, val):
    if id in data:
        return data[id][val]
    else:
        return None


def generate_random_table(table_name, columns, num_rows, cursor, last_id=None):
    def get_default_value(columnName):
        nonlocal last_id

        if columnName == "sala_id" and table_name == "Sala":
            last_id = get_next_id(cursor, 'Sala', 'sala_id', last_id)
            Sala.append(last_id)
            return last_id
        elif columnName == "nazwa_sali":  #todo?
            return fake.word() + " Sala"
        elif columnName == "kino_id" and table_name == "Sala":
            return get_random_id(cursor, 'Kino', 'kino_id', Kino)


        elif columnName == "kino_id" and table_name == "Kino":
            last_id = get_next_id(cursor, 'Kino', 'kino_id', last_id)
            Kino.append(last_id)
            return last_id
        elif columnName == "nazwa_kina":
            return fake.word() + "Kino"
        elif columnName == "lokalizacja_id" and table_name == "Kino":
            return get_random_id(cursor, 'Lokalizacja', 'lokalizacja_id', Lokalizacja)


        elif columnName == "lokalizacja_id" and table_name == "Lokalizacja":
            last_id = get_next_id(cursor, 'Lokalizacja', 'lokalizacja_id', last_id)
            Lokalizacja.append(last_id)
            return last_id
        elif columnName == "gps_x":  #todo
            return random.uniform(0.0, 100.0)
        elif columnName == "gps_y":  #todo
            return random.uniform(0.0, 100.0)
        elif columnName == "nazwa_miejscowosci":
            return fake.city()


        elif columnName == "seans_id" and table_name == "Seans":
            last_id = get_next_id(cursor, 'Seans', 'seans_id', last_id)
            Seans.append(last_id)
            return last_id
        elif columnName == "film_id" and table_name == "Seans":
            return get_random_id(cursor, 'Film', 'film_id', Film)
        elif columnName == "sala_id" and table_name == "Seans":
            return get_random_id(cursor, 'Sala', 'sala_id', Sala)
        elif columnName == "rozpoczecie_seansu":  #todo
            return random.uniform(0.0, 100.0)
        elif columnName == "cena":  #todo
            rngCena = random.uniform(0.0, 100.0)
            PomSeans[last_id] = {"seans_id": last_id, "cena": rngCena}
            return rngCena

        elif columnName == "film_id" and table_name == "Film":
            last_id = get_next_id(cursor, 'Film', 'film_id', last_id)
            Film.append(last_id)
            return last_id
        elif columnName == "minuty_filmu":  #todo
            return random.uniform(0.0, 100.0)
        elif columnName == "dla_doroslych":  #todo
            return random.randint(0, 1)
        elif columnName == "tytul":  #todo
            return fake.word()
        elif columnName == "rok_premiery":  #todo
            return fake.date()


        elif columnName == "miejsce_id" and table_name == "Miejsce":
            last_id = get_next_id(cursor, 'Miejsce', 'miejsce_id', last_id)
            Miejsce.append(last_id)
            return last_id
        elif columnName == "rzad_id":  #todo?
            return random.randint(1, 25)
        elif columnName == "siedzenie":  #todo?
            return random.randint(0, 20)
        elif columnName == "dla_niepelnosprawnych":  #todo?
            return random.randint(0, 1)
        elif columnName == "sala_id" and table_name == "Miejsce":
            return get_random_id(cursor, 'Miejsce', 'miejsce_id', Miejsce)

        elif columnName == "uzytkownik_id" and table_name == "Uzytkownik":
            last_id = get_next_id(cursor, 'Uzytkownik', 'uzytkownik_id', last_id)
            Uzytkownik.append(last_id)
            return last_id
        elif columnName == "imie":  #todo?
            return fake.first_name()
        elif columnName == "nazwisko":  #todo?
            return fake.last_name()
        elif columnName == "email":  #todo?
            return fake.email()
        elif columnName == "haslo":  #todo?
            return fake.password()
        elif columnName == "pracownik_id" and table_name == "Uzytkownik":
            return get_random_id(cursor, 'Pracownik', 'pracownik_id', Pracownik)

        elif columnName == "promocja_id" and table_name == "Promocja":
            last_id = get_next_id(cursor, 'Promocja', 'promocja_id', last_id)
            Promocja.append(last_id)
            return last_id
        elif columnName == "nazwa_promocji":  #todo?
            return "Promocja" + fake.word()
        elif columnName == "znizka":  #todo?
            rngProm = random.uniform(0, 100)
            PomProm[last_id] = {"promocja_id": last_id, "znizka": rngProm}
            return rngProm


        elif columnName == "pracownik_id" and table_name == "Pracownik":
            last_id = get_next_id(cursor, 'Pracownik', 'pracownik_id', last_id)
            Pracownik.append(last_id)
            return last_id
        elif columnName == "pensja":  #todo?
            return random.randint(4000, 7000)
        elif columnName == "czy_admin":  #todo?
            return random.randint(0, 1)

        elif columnName == "bilet_id" and table_name == "Bilet":
            last_id = get_next_id(cursor, 'Bilet', 'bilet_id', last_id)
            Bilet.append(last_id)
            return last_id
        elif columnName == "cena_koncowa":
            seans_id = row['seans_id']
            promocja_id = row['promocja_id']
            cena = pobierzDane(PomSeans, seans_id, 'cena')
            promocja = pobierzDane(PomProm, promocja_id, 'znizka')

            return cena * (1 - promocja / 100.0)
        elif columnName == "promocja_id" and table_name == "Bilet":
            return get_random_id(cursor, 'Promocja', 'promocja_id', Promocja)
        elif columnName == "seans_id" and table_name == "Bilet":
            return get_random_id(cursor, 'Seans', 'seans_id', Seans)
        elif columnName == "miejsce_id" and table_name == "Miejsce":
            return get_random_id(cursor, 'Miejsce', 'miejsce_id', Miejsce)
        elif columnName == "uzytkownik_id" and table_name == "Bilet":
            return get_random_id(cursor, 'Uzytkownik', 'uzytkownik_id', Uzytkownik)

        else:
            return fake.word()

    table_data = []

    for _ in range(num_rows):
        row = {}
        for column in columns:
            row[column] = get_default_value(column)
        table_data.append(row)

    return {table_name: table_data}, last_id


def main():
    try:
        # Nawiązanie połączenia z bazą danych MySQL
        connection = mysql.connector.connect(
            host='localhost',
            user='root',
            passwd='pass',
            database='cinemaDB'
        )

        if connection.is_connected():
            cursor = connection.cursor()

            lokal_columns = ["lokalizacja_id", "gps_x", "gps_y", "nazwa_miejscowosci"]
            kino_columns = ["kino_id", "nazwa_kina", "lokalizacja_id"]
            sala_columns = ["sala_id", "nazwa_sali", "kino_id"]
            seans_columns = ["seans_id", "film_id", "sala_id", "rozpoczecie_seansu", "cena"]
            film_columns = ["film_id", "minuty_filmu", "dla_doroslych", "tytul", "rok_premiery"]
            bilet_columns = ["bilet_id", "promocja_id", "seans_id", "miejsce_id", "uzytkownik_id", "cena_koncowa"]
            miejsce_columns = ["miejsce_id", "rzad_id", "sala_id", "siedzenie", "dla_niepelnosprawnych"]
            user_columns = ["uzytkownik_id", "imie", "nazwisko", "email", "haslo", "pracownik_id"]
            promocja_columns = ["promocja_id", "nazwa_promocji", "znizka"]
            pracownik_columns = ["pracownik_id", "pensja", "czy_admin"]

            lokal_table = {}
            kino_table = {}
            sala_table = {}
            seans_table = {}
            film_table = {}
            bilet_table = {}
            miejsce_table = {}
            user_table = {}
            promocja_table = {}
            pracownik_table = {}



            lokal_table, _ = generate_random_table("Lokalizacja", lokal_columns, 5, cursor)
            kino_table, _ = generate_random_table("Kino", kino_columns, 5, cursor)
            sala_table, _ = generate_random_table("Sala", sala_columns, 5, cursor)
            miejsce_table, _ = generate_random_table("Miejsce", miejsce_columns, 5, cursor)
            film_table, _ = generate_random_table("Film", film_columns, 2, cursor)
            seans_table, _ = generate_random_table("Seans", seans_columns, 2, cursor)
            promocja_table, _ = generate_random_table("Promocja", promocja_columns, 2, cursor)
            pracownik_table, _ = generate_random_table("Pracownik", pracownik_columns, 2, cursor)
            user_table, _ = generate_random_table("Uzytkownik", user_columns, 2, cursor)
            bilet_table, _ = generate_random_table("Bilet", bilet_columns, 2, cursor)

            dbStructure = {**lokal_table, **kino_table, **sala_table, **seans_table, **film_table, **bilet_table,
                           **miejsce_table, **user_table, **promocja_table, **pracownik_table}
            print(dbStructure)

    except Error as e:
        print(f"Błąd: {e}")

    finally:
        if connection.is_connected():
            cursor.close()
            connection.close()
            print("Połączenie z bazą danych zostało zamknięte.")


if __name__ == "__main__":
    main()
