from tkinter import filedialog

def generuj_inserty_sql_i_zapisz(dane):

    sciezka_do_pliku = filedialog.asksaveasfilename(defaultextension=".sql", filetypes=(("Pliki SQL", "*.sql"), ("Wszystkie pliki", "*.*")))
    if not sciezka_do_pliku:
        return

    with open(sciezka_do_pliku, 'w') as plik_sql:
        # Wyłączenie sprawdzania kluczy obcych
        plik_sql.write("SET FOREIGN_KEY_CHECKS = 0;\n\n")

        plik_sql.write("START TRANSACTION;\n\n")

        for typ_danych, rekordy in dane.items():
            if rekordy:
                plik_sql.write(f"-- Inserty dla tabeli: {typ_danych}\n")

                for rekord in rekordy:
                    kolumny = ', '.join(rekord.keys())
                    wartosci = ', '.join([str(wartosc) if isinstance(wartosc, (int, float)) else f"'{wartosc}'" for wartosc in rekord.values()])
                    plik_sql.write(f"INSERT INTO {typ_danych} ({kolumny}) VALUES ({wartosci});\n")

        plik_sql.write("\nCOMMIT;")
        # Włączenie sprawdzania kluczy obcych
        plik_sql.write("\nSET FOREIGN_KEY_CHECKS = 1;")