#!/usr/bin/env python3

import sys
from antlr4 import *
from parsers.antlr4src.gen.MySql.MySqlLexer import MySqlLexer
from parsers.antlr4src.gen.MySql.MySqlParser import MySqlParser
from parsers.MySqlListener import MySqlListener
from InjectionExecutor import InjectionExecutor

def main(argv):
    inputStream = FileStream(argv[1])
    lexer = MySqlLexer(inputStream)
    tokenStream = CommonTokenStream(lexer)
    parser = MySqlParser(tokenStream)
    tree = parser.root()

    listener = MySqlListener(lexer, tokenStream, parser)
    walker = ParseTreeWalker()
    walker.walk(listener, tree)

    injExec = InjectionExecutor(listener.dbStructure)
    print(injExec.execute())



if __name__ == "__main__":
    main(sys.argv)
