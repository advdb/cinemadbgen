#!/usr/bin/env bash
echo Building MySQL parser

cd src/parsers/antlr4src
antlr4 -Dlanguage=Python3 -listener -visitor MySqlLexer.g4 MySqlParser.g4 -o gen/MySql/
cd ../../..
