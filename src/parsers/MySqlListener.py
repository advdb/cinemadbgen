from antlr4.xpath.XPath import XPath
from parsers.antlr4src.gen.MySql.MySqlLexer import MySqlLexer
from parsers.antlr4src.gen.MySql.MySqlParser import MySqlParser
from parsers.antlr4src.gen.MySql.MySqlParserListener import MySqlParserListener

def stripCommentTags(string):
    # Comment tags for this are /*py [..] */
    return string[4:-2]

class MySqlListener(MySqlParserListener):
    def __init__(self, lexer, tokenStream, parser):
        self.lexer = lexer
        self.tokenStream = tokenStream
        self.parser = parser

        # {
        #     "schemaExec": <schema-exec-str>,
        #     "tables": {
        #         "<table1>": {
        #             "tableExec": <table-exec-str>,
        #             "fields": {
        #                 "<field1>": {"type": <type>, "fieldExec": <field-exec-str>},
        #                 ...
        #             },
        #             ...
        #         },
        #         ...
        #     }
        # }
        self.dbStructure = {
            "schemaExec": None,
            "tables": {}
        }

    def extractBlockFromHiddenTokens(self, hiddenTokens):
        if hiddenTokens is None:
            return None
        if len(hiddenTokens) > 1:
            print("WARN: Detected more than one python injection block.\n" +
                  "Only the last block will be executed!")
        return stripCommentTags(hiddenTokens[-1].text).strip()

    def enterCreateDatabase(self, ctx:MySqlParser.CreateDatabaseContext):
        execString = self.extractBlockFromHiddenTokens(
            self.tokenStream.getHiddenTokensToLeft(ctx.start.tokenIndex, 2))
        if execString is None:
            return
        self.dbStructure["schemaExec"] = execString

    def enterColumnCreateTable(self, ctx:MySqlParser.ColumnCreateTableContext):
        execString = self.extractBlockFromHiddenTokens(
            self.tokenStream.getHiddenTokensToLeft(ctx.start.tokenIndex, 2))

        # turns out sql names are complex; this might be enough
        tableName = ctx.tableName().fullId().uid()[0].STRING_LITERAL().getText().strip('`')

        self.dbStructure["tables"][tableName] = {
            "tableExec": execString,
            "fields": {}
        }

        createDefs = XPath.findAll(ctx, "//createDefinition", self.parser)
        # Find only those of the columnDeclaration antlr4 label
        createDefs = list(filter(
            lambda el: issubclass(el.__class__, MySqlParser.ColumnDeclarationContext),
            createDefs))

        for createDef in createDefs:
            defName = createDef.fullColumnName().uid().STRING_LITERAL().getText().strip('`')
            defExec = self.extractBlockFromHiddenTokens(
                self.tokenStream.getHiddenTokensToRight(createDef.stop.tokenIndex, 2))
            self.dbStructure["tables"][tableName]["fields"][defName] = {
                "fieldExec": defExec
                # we can collect types and other column characteristics here
            }

