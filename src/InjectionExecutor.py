
class InjectionExecutor:
    def __init__(self, dbStructure):
        self.dbStructure = dbStructure

    def execute(self):
        # {<table-name>: [{<field1>: <value>, ...}, ...], ...}
        dataToInsert = {}

        schemaCtx = {}
        if(self.dbStructure["schemaExec"] is not None):
            schemaLocals = {}
            exec(self.dbStructure["schemaExec"], globals(), schemaLocals)
            if "schemaContext" in schemaLocals:
                schemaCtx = schemaLocals["schemaContext"]()

        for tableName, tableData in self.dbStructure["tables"].items():
            tableLocals = {}
            if tableData["tableExec"] is not None:
                exec(tableData["tableExec"], globals(), tableLocals)

            tableCtx = {}
            if "tableContext" in tableLocals:
                tableCtx = tableLocals["tableContext"](schemaCtx)

            dataToInsert[tableName] = []

            rowsToExec = 0
            if "rows" in tableLocals:
                rowsToExec = tableLocals["rows"]

            for idx in range(0, rowsToExec):
                rowCtx = {}
                if "rowContext" in tableLocals:
                    rowCtx = tableLocals["rowContext"](schemaCtx, tableCtx)

                rowDataToInsert = {}
                for fieldName, fieldData in tableData["fields"].items():
                    fieldCtx = {
                        "schemaCtx": schemaCtx,
                        "tableCtx": tableCtx,
                        "rowCtx": rowCtx,
                        "rowNum": idx,
                        # also provide the "fake data generation library" functions as local vars
                    }
                    fieldValue = None
                    if fieldData["fieldExec"] is not None:
                        fieldValue = eval(fieldData["fieldExec"], globals(), fieldCtx)

                    rowDataToInsert[fieldName] = fieldValue
                dataToInsert[tableName].append(rowDataToInsert)
        return dataToInsert

